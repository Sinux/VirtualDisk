// CommandUI.cpp : 定义 DLL 应用程序的导出函数。

#include "stdafx.h"
#include "CommandUI.h"
#include <iostream>

using namespace std;

COMMANDUI_API IDiskUI* _stdcall CreateCommandUI(void)
{
	return static_cast<IDiskUI *>(new CCommandUI);
}

CCommandUI::CCommandUI()
{
	return;
}

CCommandUI::~CCommandUI()
{
	return;
}

String CCommandUI::GetCommand()
{
	String in;
	cin>>in;
	return in;
}


void CCommandUI::OutputString(String &str)
{
	cout<<str;
}

void CCommandUI::OutputString(const char *str)
{
	cout<<str;
}
