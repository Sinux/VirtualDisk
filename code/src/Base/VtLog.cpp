#ifdef WIN32

#include <winsock2.h>
#include <process.h>
#include <time.h>
#include <MMSystem.h>
#include <io.h>
#include "VtLog.h"
#include <stdio.h>

#pragma warning(disable:4996)

static char s_log_buf[0xffff];
static int s_log_buf_idx = 0;
static LMutex* s_log_buf_mtx = NULL;
static LMutex* s_log_file_mtx = NULL;
static FILE* s_fp_log = NULL;
static int s_log_file_day = -1;
static char s_log_path[MAX_PATH] = {0};
static int blWriteLog = -1;

#ifndef __BORLANDC__
#define access(A,B) _access(A,B)
#endif

#define vsnprintf _vsnprintf
#define snprintf _snprintf


static BOOL isWriteLog() {
	/*
	if (blWriteLog == -1) {
		if (access("vtlog", 0) == 0) {
			blWriteLog = 1;
		} else {
			blWriteLog = 0;
		}
	}
	return blWriteLog;	
	*/
	return 1;
}

static void
check_delete_log_file()
{
	char log_path[MAX_PATH];
	char find_filename[MAX_PATH];

	strcpy(log_path, s_log_path);
	strcpy(find_filename, s_log_path);
	strcat(find_filename,"log_*");

	WIN32_FIND_DATA find_fd;
	HANDLE find_handle = FindFirstFile(find_filename,&find_fd); 
	//INVALID_HANDLE_VALUE 查找失败返回值
	if (find_handle != INVALID_HANDLE_VALUE) {
	   do {
		    if (memcmp(find_fd.cFileName, "log_", 4) != 0){				
				continue;//过滤掉不是日志的文件
		    }
	
		    char log_time_by_name[MAX_PATH] = {0};
		    memcpy(log_time_by_name, find_fd.cFileName + 4, 10);//取名字第5位开始

			//char转换time_t格式↓
			char *name_pos = strstr(log_time_by_name,"-"); 
			if(name_pos == NULL){ 
				vtlog("");
				continue; 
			} 

			int year = atoi(log_time_by_name);
			int month = atoi(name_pos + 1);
			name_pos = strstr(name_pos + 1,"-"); 
			if(name_pos == NULL){ 
				vtlog("");
				continue; 
			}
			int day = atoi(name_pos + 1); 

			struct tm log_tm; 
			memset(&log_tm,0,sizeof(log_tm));
			log_tm.tm_mday = day; 
			log_tm.tm_mon = month - 1; 
			log_tm.tm_year = year - 1900; 
			time_t log_time = mktime(&log_tm);//取出的日志时间
				
			if (log_time == -1){
				continue;
			}

			time_t now;
			time(&now);
			//日志最长保留时间为7天
			if (now - log_time > 3600*24*7){
				char filename[MAX_PATH];
				sprintf(filename, "%s%s", log_path, find_fd.cFileName);
				remove(filename);
			}
	   }while(FindNextFile(find_handle, &find_fd));
	   FindClose(find_handle);
	}
}



BOOL STDCALL
os_mutex_init(LMutex* mutex, BOOL recursive)
{
    InitializeCriticalSection(&mutex->section);
    return TRUE;
}

LMutex* STDCALL
os_mutex_create(BOOL recursive)
{
	LMutex* mutex = (LMutex*)malloc(sizeof(LMutex));
	if (mutex == NULL) {
		vtlog("mutex is null");
		return NULL;
	}

	if (!os_mutex_init(mutex, recursive)) {
		vtlog("!os_mutex_init(mutex, recursive)");
		free(mutex);
		return NULL;
	}

	return mutex;
}

BOOL STDCALL
os_mutex_free(LMutex* mutex)
{
	os_mutex_destroy(mutex);
	free(mutex);

	return TRUE;
}

BOOL STDCALL
os_mutex_destroy(LMutex* mutex)
{
    DeleteCriticalSection(&mutex->section);
    return TRUE;
}

BOOL STDCALL
os_mutex_lock(LMutex* mutex)
{
    EnterCriticalSection(&mutex->section);
    return TRUE;
}

BOOL STDCALL
os_mutex_unlock(LMutex* mutex)
{
    LeaveCriticalSection(&mutex->section);
    return TRUE;
}


static void
err_doit_unlock(int32 errnoflag, int32 error, const char *fmt, va_list ap)
{
	char tmp[0xffff];
	memcpy(tmp, s_log_buf, s_log_buf_idx);
	int idx = s_log_buf_idx;
	s_log_buf_idx = 0;
	os_mutex_unlock(s_log_buf_mtx);

	int ret = vsnprintf(tmp + idx, 0xFFFF, fmt, ap);
	idx += ret;

	if (errnoflag) {
		ret = sprintf(tmp + idx, ": %s", strerror(error));
		idx += ret;
	}
	sprintf(tmp + idx, "\n");

	fflush(stdout);
	fputs(tmp, stderr);
	fflush(stderr);

	if (isWriteLog()) {
		time_t now;
		time(&now);
		struct tm timenow = {0};
		struct tm* tmp_time = localtime(&now);
		if (tmp_time != NULL) {
			timenow = *tmp_time;
		}

		if (s_log_file_mtx == NULL) {
			s_log_file_mtx = os_mutex_create(FALSE);
		}

		os_mutex_lock(s_log_file_mtx);
		if (s_fp_log == NULL) {
			char filename[0xffff];
			sprintf(filename, "%slog_%04d-%02d-%02d.log", s_log_path, timenow.tm_year + 1900, timenow.tm_mon + 1, timenow.tm_mday);
			s_fp_log = fopen(filename, "a");
			s_log_file_day = timenow.tm_mday;
			check_delete_log_file();
		}

		if (s_log_file_day != timenow.tm_mday) {
			if (s_fp_log != NULL) {
				fclose(s_fp_log);
			}
			char filename[0xffff];
			sprintf(filename, "%slog_%04d-%02d-%02d.log", s_log_path, timenow.tm_year + 1900, timenow.tm_mon + 1, timenow.tm_mday);
			s_fp_log = fopen(filename, "a");
			s_log_file_day = timenow.tm_mday;
			check_delete_log_file();
		}

		if (s_fp_log != NULL) {
			fputs(tmp, s_fp_log);
			fflush(s_fp_log);
		}
		os_mutex_unlock(s_log_file_mtx);
	}
#ifdef _WIN32
	::OutputDebugString(tmp);
#endif
}

X_API void STDCALL
log_buf_lock()
{
	if (s_log_buf_mtx == NULL) {
		s_log_buf_mtx = os_mutex_create(FALSE);
	}

	os_mutex_lock(s_log_buf_mtx);
}

X_API void STDCALL
log_buf_date()
{
	time_t now;
	time(&now);
	struct tm timenow = {0};
	struct tm* tmp = localtime(&now);
	if (tmp != NULL) {
		timenow = *tmp;
	}

	int ret = sprintf(s_log_buf + s_log_buf_idx, "%04d-%02d-%02d %02d:%02d:%02d ", timenow.tm_year + 1900
		, timenow.tm_mon + 1, timenow.tm_mday, timenow.tm_hour, timenow.tm_min
		, timenow.tm_sec);

	s_log_buf_idx += ret;
}

X_API void STDCALL
log_buf_msg(const char *fmt, ...)
{
	va_list		ap;

	va_start(ap, fmt);
	int ret = vsnprintf(s_log_buf + s_log_buf_idx, 0xFFFF, fmt, ap);
	s_log_buf_idx += ret;

	va_end(ap);
}

X_API void STDCALL
log_buf_msg_unlock(const char *fmt, ...)
{
	va_list	ap;
	va_start(ap, fmt);
	err_doit_unlock(0, 0, fmt, ap);
	va_end(ap);
}

X_API void STDCALL
log_get_path(OUT char* path)
{
	strcpy(path, s_log_path);
}

X_API void STDCALL
log_set_path(char* path)
{
	strcpy(s_log_path, path);
}

#endif