#include <memory.h>
#include <stdio.h>
#include <string.h>
#include <stdexcept>
#include <shlwapi.h>

#include "CString.h"

using namespace std;

#pragma warning(disable : 4996)		//���ξ���
#pragma comment(lib,"shlwapi.lib")

//���캯��
String::String(void)
{
	m_length = 0;
	m_str = new char(0);
}

String::String(const char * str)
{
	String();

	if (NULL != str)
	{
		m_length = strlen(str);
		m_str = new char[m_length + 1];
		if (NULL == m_str)
		{
			throw runtime_error("�����ڴ�ʧ��");
		}		
		memcpy(m_str, str, m_length + 1);
	}
}
String::String(String &str)
{
	m_length = str.length();
	m_str = new char[m_length + 1];
	if (NULL == m_str)
	{
		throw runtime_error("�����ڴ�ʧ��");
	}

	memcpy(m_str, str.c_str(), m_length + 1);
}

//��������
String::~String(void)
{
	if(NULL != m_str)
	{
		delete [] m_str;
		m_str = NULL;
		m_length = 0;
	}
}

//��ֵ����
String & String::operator = (String &str)
{
	if (this == &str)
	{
		return *this;
	}
	
	delete [] m_str;
	m_str = NULL;
	m_length = str.length();
	m_str = new char[m_length + 1];
	if (NULL == m_str)
	{
		throw runtime_error("����ռ�ʧ��");
	}
	memcpy(m_str, str.c_str(), m_length + 1);

	return *this;
}
String & String::operator = (const char * str)
{
	if (NULL == str)
	{
		throw runtime_error("�쳣�Ŀ�ָ��");
	}

	if (m_str == str)
	{
		return *this;
	}

	delete [] m_str;
	m_length = strlen(str);
	m_str = new char[m_length + 1];
	if (NULL == m_str)
	{
		throw runtime_error("����ռ�ʧ��");
	}
	memcpy(m_str, str, m_length + 1);

	return *this;
}

//���������
char & String::operator [](unsigned index)
{
	if (index < m_length)
	{
		return m_str[index];
	}

	throw runtime_error("�����±�Խ��");
}

bool String::operator == (String &str)
{
	return !stricmp(m_str, str.c_str());
}

bool String::operator == (const char * str)
{
	return !stricmp(m_str, str);
}

bool String::operator != (String &str)
{
	return stricmp(m_str,str.c_str()) == 0 ? false : true;
}

bool String::operator != (const char * str)
{
	return stricmp(m_str,str)  == 0 ? false : true;
}

//��ͨ�Ĺ��ܺ���
unsigned String::length(void)
{
	return m_length;
}

const char * String::c_str()
{
	return m_str;
}

String & String::operator + (String &str)
{
	return append(str.c_str());
}

String & String::operator + (const char * str)
{
	return append(str);
}

String & String::append(const char *str)
{
	m_length = m_length + strlen(str);
	char *temp = new char[m_length + 1];
	if (NULL == temp)
	{
		throw runtime_error("����ռ�ʧ��");
	}
	strcpy(temp, m_str); 
	strcat(temp, str);

	delete [] m_str;
	m_str = temp;

	return *this;
}

bool String::empty(void)
{
	return m_length == 0 ? true : false;
}

/*
	@brief ����ָ�����ַ� �����±�
*/
int String::find(const char ch)
{
	for (int i = 0; m_str[i]; i++)
	{
		if (m_str[i] == ch)
		{
			return i;
		}
	}

	return -1;
}

int String::find(String &str)
{
	return -1;
}

int String::find(const char *str)
{
	return -1;
}

String String::substr(unsigned uBegin, unsigned uEnd)
{
	String str;

	if(0 == uEnd)
	{
		str = (m_str + uBegin);
	}
	else if (uBegin < uEnd && uEnd <= m_length)
	{
		char temp = m_str[uEnd];
		m_str[uEnd] = 0;
		str = (m_str + uBegin);
		m_str[uEnd] = temp;
	}

	return str;
}

void String::trim(void)
{

	StrTrim(m_str, " ");
	m_length = strlen(m_str);
}

int String::rfind(const char ch)
{
	for (int i = m_length - 1; i >= 0; i--)
	{
		if (m_str[i] == ch)
		{
			return i;
		}
	}

	return -1;
}

int String::rfind(String &str)
{
	return -1;
}

int String::rfind(const char *str)
{
	return -1;
}

unsigned String::replace(char old, char ch)
{
	unsigned uCou = 0;
	for (unsigned i = 0; i < m_length; i++)
	{
		if (m_str[i] == old)
		{
			m_str[i] = ch;
			uCou ++;
		}
	}

	return uCou;
}
