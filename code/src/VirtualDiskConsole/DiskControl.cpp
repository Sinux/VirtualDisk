#include "DiskControl.h"
#include "DiskUI.h"
#include "CommandUI.h"
#include <iostream>
#include <Windows.h>

#include "Tree.h"
#include "CString.h"
#include "ValueTypes.h"

using namespace std;

CDiskControl::CDiskControl(void)
{
	//有继承关系的类不能memset(this, 0x00, sizeof(类)) 容易把虚表干掉 ~ ~
	m_pCommandInfo = NULL;
	m_uCommandNum = 0;
	m_pDiskStruct = NULL;
	m_pDiskUI = NULL;
	m_bDone = false;
}

CDiskControl::~CDiskControl(void)
{
	if (m_pDiskUI)
	{
		delete m_pDiskUI;
		m_pDiskUI = NULL;
	}

	if (m_pDiskStruct)
	{
		delete m_pDiskStruct;
		m_pDiskStruct = NULL;
	}

	ReleaseCommandDLL();

	if (m_pCommandInfo)
	{
		delete [] m_pCommandInfo;
		m_pCommandInfo = NULL;
	}
}

/*
	@brief初始化其它模块，组装软件结构
*/
bool CDiskControl::Init()
{
	m_pDiskUI = CreateCommandUI();
	if (NULL == m_pDiskUI)
	{
		cerr<<"DiskUI 创建失败"<<endl;
		return false;
	}
	ShowString("虚拟磁盘 [版本 1.0.0]\n");
	ShowString("版权所有 (c) 2015 Horizon Corporation。保留所有权利。\n\n");

	m_pDiskStruct = CreateTree();
	if (NULL == m_pDiskStruct)
	{
		cerr<<"DiskStruct 创建失败"<<endl;
		return false;
	}
	m_pDiskStruct->Init(this);

	String strFile;
#ifdef _DEBUG
	strFile = "*Commandd.dll";
#else
	strFile = "*Command.dll";
#endif

	if(!InitCommandDLL(strFile))
	{
		cerr<<"加载命令的动态库失败"<<endl;
		return false;
	}

	return true;
}

/*
	@brief命令系统启动
*/
void CDiskControl::Start()
{
	String str;
	String input = "help";
	m_bDone = true;	//启动
	ShowHelp();
	while(m_bDone)
	{
		// QA 为什么不能直接调用ShowString	因为虚表被覆盖了 - -！
		///一直获取获取命令
		ShowString(m_pDiskStruct->GetCurDir());			//显示当前目录
		input = m_pDiskUI->GetCommand();				//获取命令		
		DealCommand(input);								//处理命令
	}
}

void CDiskControl::Stop()
{
	m_bDone = false;	//停止，暂时不用考虑线程安全的问题
}

/*
	@brief分解命令和参数 并根据命令做成处理
*/
bool CDiskControl::DealCommand(String &str)
{
	//todo分开命令和参数
	//根据命令做出指定的动作

	bool bFlag = true;

	str.trim();	//去掉首尾的空格

	unsigned uSpace = str.find(' ');
	String strCommand, strArgs;
	if (-1 != uSpace)
	{
		strCommand = str.substr(0, uSpace);
		strArgs = str.substr(uSpace + 1);
	}
	else
	{
		strCommand = str;
	}
	
	strArgs.trim();

	if (strCommand == "exit")
	{
		Stop();
	}
	else if (strCommand == "help")
	{
		ShowHelp();
	}
	else if (strCommand == "cls")
	{
		system("cls");
	}
	else if (strCommand.empty())
	{
		bFlag = true;
	}
	else
	{
		bFlag = Execute(strCommand, strArgs);
	}

	return bFlag;
}
void CDiskControl::ShowHelp(void)
{
	ShowString("有关某个命令的详细信息，请键入 命令名 ?\n");
	for( unsigned i = 0; i < m_uCommandNum; i++)
	{
		ShowString(m_pCommandInfo[i].m_ptrCommand->Describe());
	}
	ShowString("HELP                    显示当前的帮助信息\n");
}

/*
	@brief动态加载每个命令的动态库
*/
bool CDiskControl::InitCommandDLL(String &strName)
{
	//拼合要查找的dll名称
	String strFilePath;
	GetModulePath(strFilePath);
	strFilePath = strFilePath + "\\" + strName;

	unsigned uNum = GetDllCount(strFilePath);
	m_pCommandInfo = new DllInfo[uNum];
	if (NULL == m_pCommandInfo)
	{
		throw runtime_error("申请空间失败");
	}
	
	//循环加载dll
	WIN32_FIND_DATAA wfd;
	HANDLE hFind = FindFirstFile(strFilePath.c_str(), &wfd);
	if(INVALID_HANDLE_VALUE == hFind || 0 == uNum)
		return false;
	
	unsigned uIndex = 0;

	do
	{
		HMODULE hModule = (HMODULE)LoadLibrary(wfd.cFileName);
		if(hModule == NULL)
			return false;

		pfnCreateCommand create = (pfnCreateCommand)GetProcAddress(hModule, "CreateCommand");
		pfnGetCommandName getName = (pfnGetCommandName)GetProcAddress(hModule, "GetCommandName");
		if (NULL == create || NULL == getName)
		{
			FreeLibrary(hModule);
			hModule = NULL;

			//打印日志
			continue;
		}

		m_pCommandInfo[uIndex].m_hModule = hModule;
		m_pCommandInfo[uIndex].m_pfnCreateCommand = create;
		m_pCommandInfo[uIndex].m_pfnGetCommandName = getName;
		m_pCommandInfo[uIndex].m_ptrCommand = create(this);
		m_pCommandInfo[uIndex].m_sName = getName();
		

		if (NULL == m_pCommandInfo[uIndex].m_ptrCommand ||
			m_pCommandInfo[uIndex].m_sName.empty())
		{
			if (NULL != m_pCommandInfo[uIndex].m_ptrCommand)
			{
				delete m_pCommandInfo[uIndex].m_ptrCommand;
				m_pCommandInfo[uIndex].m_ptrCommand = NULL;
			}
			
			FreeLibrary(hModule);
			hModule = NULL;

			//打印日志 报错
			continue;
		}

		uIndex ++;

	}while(FindNextFile(hFind, &wfd) && uIndex < uNum);

	FindClose( hFind);

	m_uCommandNum = uIndex;

	if (0 == uIndex)
	{
		return false;
	}

	return true;
}

/*
	@brief释放动态库
*/
void CDiskControl::ReleaseCommandDLL(void)
{
	//析构命令对象 释放dll
	if (NULL != m_pCommandInfo)
	{
		for (unsigned i = 0; i < m_uCommandNum; i++)
		{
			if (NULL != m_pCommandInfo[i].m_ptrCommand)
			{
				delete m_pCommandInfo[i].m_ptrCommand;
				m_pCommandInfo[i].m_ptrCommand = NULL;
			}

			if (NULL != m_pCommandInfo[i].m_hModule)
			{
				FreeLibrary(m_pCommandInfo[i].m_hModule);
				m_pCommandInfo[i].m_hModule = NULL;
			}
		}
	}
}

/*
	@brief获取模块的路径
*/
bool CDiskControl::GetModulePath(String &str)
{
	//获取文件路径
	char strPath[MAX_PATH];
	GetModuleFileName(NULL, strPath, MAX_PATH);
	*strrchr(strPath, '\\') = 0;

	str = strPath;

	return true;
}

/*
	@brief获取通配名称(如:comm*.dll)的dll个数
*/
unsigned CDiskControl::GetDllCount(String &strFileName)
{
	unsigned uDllNum = 0;
	WIN32_FIND_DATAA wfd;
	HANDLE hFind = FindFirstFile(strFileName.c_str(), &wfd);

	if(INVALID_HANDLE_VALUE == hFind)
		return uDllNum;

	do
	{
		uDllNum++;
	}while(FindNextFile(hFind, &wfd));

	FindClose( hFind);

	return uDllNum;
}

bool CDiskControl::ShowString(const char *str)
{
	m_pDiskUI->OutputString(str);

	return true;
}

bool CDiskControl::ShowString(String &str)
{
	m_pDiskUI->OutputString(str);
	return true;
}

bool CDiskControl::Execute(String &strCommand, String &strArgs)
{
	unsigned i;
	for( i = 0; i < m_uCommandNum; i++)
	{
		if (m_pCommandInfo[i].m_sName == strCommand)
		{
			m_pCommandInfo[i].m_ptrCommand->Execute(strArgs.c_str());
			break;
		}
	}

	if (i >= m_uCommandNum)
	{
		m_pDiskUI->OutputString("不能识别的命令\n");
		return false;
	}

	return true;
}

bool CDiskControl::ChangeDir(const char *str)
{
	return m_pDiskStruct->ChangeDir(str);
}

bool CDiskControl::CreateDir(const char *str)
{
	return m_pDiskStruct->CreateDir(String(str));
}

bool CDiskControl::ShowCurDirContent(const char *str)
{
	return m_pDiskStruct->ShowCurDirContent(str);
}

bool CDiskControl::ReMoveDir(const char *str)
{
	return m_pDiskStruct->ReMoveDir(str);
}

bool CDiskControl::CopyTrueFile(const char *str)
{
	return m_pDiskStruct->CopyTrueFile(str);
}

bool CDiskControl::DelFile(const char *str)
{
	return m_pDiskStruct->DelFile(str);
}

bool CDiskControl::CompareFile(const char *str)
{
	return m_pDiskStruct->CompareFile(str);
}
