#include "vld.h"
#include <stdio.h>
#include <crtdbg.h>
#include "CString.h"
#include "DiskControl.h"

#include "VtLog.h"

void StringTest()
{
	String s;
	cin>>s;

	String s1("xyz");
	
	String s2(s);
	cout<<s<<s1<<s2;
}

int main(int argc, char *argv[])
{
	log_set_path("d:\\log\\");	//设置日志的目录

	CDiskControl *ctrl = new CDiskControl();
	if(!ctrl->Init())
	{
		cout<<"程序初始化失败";
	}
	else
	{
		ctrl->Start();
	}
	
	delete ctrl;
	ctrl = NULL;
	
	return 0;
}