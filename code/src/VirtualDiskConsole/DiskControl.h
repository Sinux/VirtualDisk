#ifndef DISKCONTROL_H
#define DISKCONTROL_H
#include "Command.h"
#include "DiskStruct.h"
#include "DiskUI.h"
#include "CString.h"
#include "Control.h"


#include <Windows.h>

class CDiskControl;

typedef __declspec( dllimport ) ICommand* (__cdecl *pfnCreateCommand)(IControl *);
typedef __declspec( dllimport ) const char * (__cdecl *pfnGetCommandName)(void);

struct DllInfo
{
	String m_sName;							//名称
	ICommand*  m_ptrCommand;				//命令对象

	HMODULE	m_hModule;
	pfnCreateCommand  m_pfnCreateCommand;	//创建函数
	pfnGetCommandName m_pfnGetCommandName;	//获取名称
};


class CDiskControl : public IControl
{
private:
	DllInfo *m_pCommandInfo;		//命令数组
	unsigned m_uCommandNum;			//命令的个数
	IDiskStruct *m_pDiskStruct;		//数据结构
	IDiskUI *m_pDiskUI;				//交互
	bool m_bDone;					//启动、停止标示

public:
	CDiskControl(void);
	~CDiskControl(void);
	bool Init();
	void Start();
	void Stop();
	bool DealCommand(String &);
	void ShowHelp(void);
	bool Execute(String&, String&);

	//实现
	virtual bool ShowString(const char *);
	virtual bool ShowString(String &);
	virtual bool ChangeDir(const char *);
	virtual bool CreateDir(const char *);
	virtual bool ShowCurDirContent(const char *);
	virtual bool ReMoveDir(const char *);
	virtual bool CopyTrueFile(const char *);
	virtual bool DelFile(const char *);
	virtual bool CompareFile(const char *);

private:
	bool InitCommandDLL(String &);
	void ReleaseCommandDLL(void);
	bool GetModulePath(String &);
	unsigned GetDllCount(String &strFileName);
};

#endif