// RmdirCommand.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "RmdirCommand.h"

CRmdirCommand::CRmdirCommand(IControl *ctrl)
{
	m_pControl = ctrl;
}

CRmdirCommand::~CRmdirCommand()
{
	
}

bool CRmdirCommand::Execute(const char *str, ...)
{
	char temp[512];
	if (strcmp(str, "?") == 0)
	{
		Help();
	}
	else
	{
		if (!CheckParam(str))
		{
			sprintf(temp, "错误的参数: %s\n", str);
			m_pControl->ShowString(temp);
		}

		if (!m_pControl->ReMoveDir(str))
		{
			sprintf(temp, "错误的参数: %s\n", str);
			m_pControl->ShowString(temp);
		}
	}
	return true;
}

const char * CRmdirCommand::Describe()
{
	return "RMDIR\t\t\t删除目录。\n";
}

void CRmdirCommand::Help()
{
	m_pControl->ShowString("删除一个目录。\n\nRMDIR [/S] path\n \
S\t除目录本身外，还将删除指定目录下的所有子目录和文件。用于删除目录树。\n");
}

bool CRmdirCommand::CheckParam(const char *str)
{
	return true;
}

RMDIRCOMMAND_API const char * GetCommandName(void)
{
	return "rmdir";
}

RMDIRCOMMAND_API ICommand * CreateCommand(IControl *ctrl)
{
	return static_cast<ICommand*>(new CRmdirCommand(ctrl));
}
