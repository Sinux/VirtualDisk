// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 RMDIRCOMMAND_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// RMDIRCOMMAND_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。
#ifndef RMDIRCOMMAND_H
#define RMDIRCOMMAND_H

#include "command.h"

#ifdef RMDIRCOMMAND_EXPORTS
#define RMDIRCOMMAND_API extern "C" __declspec(dllexport)
#else
#define RMDIRCOMMAND_API extern "C" __declspec(dllimport)
#endif

class CRmdirCommand : public ICommand
{
public:
	CRmdirCommand(IControl *);
	~CRmdirCommand();

	//重写
	virtual bool Execute(const char *, ...);
	virtual const char *Describe();
	virtual void Help();
	virtual bool CheckParam(const char *);
};


RMDIRCOMMAND_API const char * GetCommandName(void);

RMDIRCOMMAND_API ICommand * CreateCommand(IControl *);

#endif	//RMDIRCOMMAND_H