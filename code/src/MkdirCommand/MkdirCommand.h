// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 MKDIRCOMMAND_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// MKDIRCOMMAND_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。

#ifndef MKDIRCOMMAND_H
#define MKDIRCOMMAND_H

#include "Command.h"

#ifdef MKDIRCOMMAND_EXPORTS
#define MKDIRCOMMAND_API extern "C" __declspec(dllexport)
#else
#define MKDIRCOMMAND_API extern "C" __declspec(dllimport)
#endif


class CMkdirCommand : public ICommand{
public:
	CMkdirCommand(IControl *);
	~CMkdirCommand(){};
	
	//重写
	virtual bool Execute(const char *, ...);
	virtual const char *Describe(void);
	virtual void Help();
	virtual bool CheckParam(const char *);
};

MKDIRCOMMAND_API const char *GetCommandName(void);
MKDIRCOMMAND_API ICommand *CreateCommand(IControl *);

#endif	//MKDIRCOMMAND_H
