// MkdirCommand.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "MkdirCommand.h"


MKDIRCOMMAND_API const char * GetCommandName(void)
{
	return "mkdir";
}

MKDIRCOMMAND_API ICommand *CreateCommand(IControl *ctrl)
{
	return static_cast<ICommand*>(new CMkdirCommand(ctrl));
}

CMkdirCommand::CMkdirCommand(IControl *ctrl)
{
	m_pControl = ctrl;
}

bool CMkdirCommand::Execute(const char *str, ...)
{
	char temp[512];
	//todo 根据参数创建目录
	if (strcmp(str, "?") == 0)
	{
		Help();
	}
	else
	{
		if (strcmp(str, "") == 0)
		{
			sprintf(temp, "文件夹名称不能为空\n");
			m_pControl->ShowString(temp);
			return false;
		}
		
		if (!CheckParam(str))
		{
			sprintf(temp, "文件夹名称不能含有非法字符: \\/:*?\"<>|\n");
			m_pControl->ShowString(temp);
			return false;
		}

		if(!m_pControl->CreateDir(str))
		{
			sprintf(temp, "创建文件夹失败: %s\n", str);
		}
	}

	return true;
}

const char * CMkdirCommand::Describe()
{
	return "MKDIR\t\t\t创建一个目录。\n";
}

void CMkdirCommand::Help()
{
	m_pControl->ShowString("创建目录。\nMKDIR path\n");
}

bool CMkdirCommand::CheckParam(const char *str)
{

	if(strcmp(str, "..") == 0 || strcmp(str, ".") == 0)
	{
		return false;
	}

	char arr[] = "\\/:*?\"<>|";
	for (int i = 0; str[i]; i++)
	{
		if (strchr(arr, str[i]) != NULL)
		{
			return false;
		}
	}

	return true;
}
