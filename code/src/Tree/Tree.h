// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 TREE_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// TREE_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。
#ifndef TREE_H
#define TREE_H

#include "DiskStruct.h"
#include "TreeNode.h"

#ifdef TREE_EXPORTS
#define TREE_API extern "C" __declspec(dllexport)
#else
#define TREE_API extern "C" __declspec(dllimport)
#endif

class CTree : public IDiskStruct
{
private:
	CTreeNode *m_treeRoot;		//树根
	CTreeNode * m_pCurDir;		//当前活动的文件夹
	CTreeNode * m_pCurFile;		//当前的文件 传参用
	String m_strActiveDirName;	//活动目录的全路径
public:
	CTree(void);
	~CTree();

	//重写
	virtual bool ChangeDir(const char *);
	virtual String &GetCurDir();
	virtual bool Init(IControl *);
	virtual bool Add();
	virtual bool CreateDir(String &);
	virtual bool ShowCurDirContent(const char *str);
	virtual bool ReMoveDir(const char *);
	virtual bool CopyTrueFile(const char *);
	virtual bool DelFile(const char *);
	virtual bool CompareFile(const char *);

	//普通成员
	bool Add(CTreeNode *, String &, NODE_TYPE);
	bool DelNode(CTreeNode *, const char*, NODE_TYPE);
	void ShowTree(void);
	void UpdateCurDir(const char *, unsigned);
	bool DestroyTree(void);
	CTreeNode *GetCurNode(void);
	void ShowString(const char *);
	CTreeNode *FindNode(const char *, NODE_TYPE);
	CTreeNode *FindNode(const char *, NODE_TYPE, CTreeNode *);
	bool DelDeep();
	void DelAllFile();
	bool CopyAll(const char *);
	bool CopyOneFile(const char *, const char *);

};

TREE_API IDiskStruct * CreateTree();

#endif // TREE_H