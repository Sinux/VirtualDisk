#include "stdafx.h"
#include "vld.h"
#include "Tree.h"

#pragma comment(linker, "/subsystem:console")  //生成控制台的

int main()
{
	CTree *t = new CTree();
	t->Init(NULL);
	t->Add(t->GetCurNode(),String("abcd"), NODE_DIR);
	t->Add(t->GetCurNode(),String("123"), NODE_DIR);
	t->ChangeDir("abcd");
	

	t->ShowTree();
	delete t;

	return 0;
}