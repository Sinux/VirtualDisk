#ifndef TREENODE_H
#define TREENODE_H

#pragma warning(disable : 4996)		//屏蔽警告

#include "ValueTypes.h"
#include "CString.h"

class CTreeNode
{
#define RATE	4				//节点空间增长速度
public:
	String m_strData;			//数据
	CTreeNode **m_ptrChild;		//孩子结点指针的数组
	unsigned m_uChildCount;		//孩子的数量
	NODE_TYPE m_emNodeType;		//节点类型
	unsigned m_uNodeSum;		//申请的空间总数
	char *m_buff;				//存放文件内容
	unsigned m_uBufLen;			//buff的大小
	CTreeNode *m_ptrParent;		//父亲指针
	
public:
	CTreeNode();
	~CTreeNode(void);

	//操作符重载
	CTreeNode *operator [](unsigned );

	//普通函数
	void SetData(String &);
	void SetData(const char *);
	String &GetData(void);
	
	void SetChildCount(unsigned);
	unsigned GetChildCount(void);

	void ShowNode(char *, unsigned);
	void ShowNodeDeep(CTreeNode *, char *, unsigned, unsigned &, unsigned &, unsigned &, const char *);
	
};

#endif	//TREENODE_H