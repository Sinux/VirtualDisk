#include "StdAfx.h"
#include "TreeNode.h"

#include <stdexcept>

using namespace std;

CTreeNode::CTreeNode()
{
	m_uChildCount = 0;
	m_ptrChild = NULL;
	m_emNodeType = NODE_DIR;
	m_uNodeSum = 0;
	m_buff = NULL;
	m_uBufLen = NULL;
	m_ptrParent = NULL;
}

CTreeNode::~CTreeNode(void)
{
	//循环release每个孩子节点
	if (m_uChildCount > 0 && NULL != m_ptrChild)
	{
		do
		{
			
			delete m_ptrChild[m_uChildCount - 1];
			m_ptrChild[m_uChildCount - 1] = NULL;
			
		}while(-- m_uChildCount > 0);
	}
	if (NULL != m_ptrChild)
	{
		delete m_ptrChild;				//delete [] 和 delete 二级指针的区别 ?
		m_ptrChild = NULL;
	}

	if (NULL != m_buff)
	{
		delete [] m_buff;
		m_buff = NULL;
		m_uBufLen = 0;
	}
}

//操作符重载
CTreeNode * CTreeNode::operator [](unsigned index)
{
	if (index < m_uChildCount)
	{
		return m_ptrChild[index];
	}

	throw runtime_error("数组下表越界");
}

//普通函数
void CTreeNode::SetData(String &str)
{
	m_strData = str;
}

void CTreeNode::SetData(const char * str)
{
	m_strData = str;
}

String &CTreeNode::GetData(void)
{
	return m_strData;
}

void CTreeNode::SetChildCount(unsigned count)
{
	m_uChildCount = count;
}
unsigned CTreeNode::GetChildCount(void)
{
	return m_uChildCount;
}

/*
 *
 *type的不同值决定调用的方式:
 *0参数为空，只显示但前目录内容
 *1递归汇总
 *2只显示目录
 *3只显示文件
 *4递归只显示目录
 *5递归只显示文件
 *
 */
void CTreeNode::ShowNode(char *str, unsigned type)
{
	unsigned uDirNum = 2;		//目录数
	unsigned uFileNum = 0;		//文件数
	unsigned uTotalBufNum = 0;	//字节总数

	char temp[1024];

	for (unsigned i = 0; i < m_uChildCount; i++)
	{
		if (NODE_DIR == m_ptrChild[i]->m_emNodeType && type != 3 && type != 5)
		{
			sprintf(temp, "\t<DIR>\t\t%s\n", m_ptrChild[i]->m_strData.c_str());
			strcat(str, temp);
			uDirNum++;
		}

		if (NODE_FILE == m_ptrChild[i]->m_emNodeType && type != 2 && type != 4)
		{
			sprintf(temp, "\t%d\t\t%s\n", m_ptrChild[i]->m_uBufLen, m_ptrChild[i]->m_strData.c_str());
			strcat(str, temp);
			uFileNum ++;
			uTotalBufNum += m_ptrChild[i]->m_uBufLen;
		}
	}

	sprintf(temp, "\t%d 个文件     %d字节\n\t%d 个目录\n", uFileNum, uTotalBufNum, uDirNum);
	strcat(str, temp);
}

void CTreeNode::ShowNodeDeep(CTreeNode *node, char *str, unsigned type, unsigned &file, unsigned &dir, unsigned &buf, const char *curname)
{
	unsigned uDirNum = 2;		//目录数
	unsigned uFileNum = 0;		//文件数
	unsigned uTotalBufNum = 0;	//字节总数
	char temp[1024];
	char name[512];

	sprintf(temp, "\n%s 的目录\n\n\t<DIR>\t\t.	\n\t<DIR>\t\t..	\n",curname);
	strcat(str, temp);
	for (unsigned i = 0; i < node->m_uChildCount; i++)
	{
		if (NODE_DIR == node->m_ptrChild[i]->m_emNodeType && type != 3 && type != 5)
		{
			sprintf(temp, "\t<DIR>\t\t%s\n", node->m_ptrChild[i]->m_strData.c_str());
			strcat(str, temp);
			uDirNum++;			
		}

		if (NODE_FILE == node->m_ptrChild[i]->m_emNodeType && type != 2 && type != 4)
		{
			sprintf(temp, "\t%d\t\t%s\n", node->m_ptrChild[i]->m_uBufLen, node->m_ptrChild[i]->m_strData.c_str());
			strcat(str, temp);
			uFileNum ++;
			uTotalBufNum += node->m_ptrChild[i]->m_uBufLen;
		}
	}

	sprintf(temp, "\t%d 个文件     %d字节\n\t%d 个目录\n", uFileNum, uTotalBufNum, uDirNum);
	strcat(str, temp);

	file += uFileNum;
	dir += uDirNum;
	buf += uTotalBufNum;


	for (unsigned k = 0; k < node->m_uChildCount; k++)
	{
		if (NODE_DIR == node->m_ptrChild[k]->m_emNodeType)
		{
			strcpy(name, curname);
			if (name[strlen(name) - 1] != '\\')
			{
				strcat(name, "\\");
			}
			strcat(name, node->m_ptrChild[k]->m_strData.c_str());
			ShowNodeDeep(node->m_ptrChild[k], str, type, file, dir, buf, name);
		}
	}
}
