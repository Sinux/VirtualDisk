// Tree.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include <io.h>
#include <iostream>
#include <fstream>
#include "Tree.h"

using namespace std;

CTree::CTree()
{
	m_treeRoot = new CTreeNode();
	m_treeRoot->SetData("C:\\");		//根目录
	m_pCurDir = m_treeRoot;
	m_strActiveDirName = m_treeRoot->m_strData;
	m_strActiveDirName.append(">");
	m_pCurFile = NULL;
}
CTree::~CTree()
{
	//销毁树
	DestroyTree();
}

bool CTree::Init(IControl *ctrl)
{
	m_pCtrl = ctrl;
	return true;
}

bool CTree::Add()
{
	return true;
}

bool CTree::Add(CTreeNode *root, String &strName, NODE_TYPE type)
{
	if (FindNode(strName.c_str(), type, root) != NULL)
	{
		ShowString("\n\t\t已经存在了\n");
		return false;
	}

	CTreeNode *node = new CTreeNode();
	node->m_strData = strName;
	node->m_emNodeType = type;
	

	if (root->m_uNodeSum <= root->m_uChildCount)
	{
		CTreeNode **temp = new CTreeNode *[root->m_uNodeSum + RATE];
		if (NULL != root->m_ptrChild)
		{
			memcpy(temp, root->m_ptrChild, sizeof(CTreeNode *) * root->m_uNodeSum);
			if (NULL != root->m_ptrChild)
			{
				delete root->m_ptrChild;
			}
		}
		root->m_ptrChild = temp;
		root->m_uNodeSum += RATE;
	}
	root->m_ptrChild [root->m_uChildCount++] = node;
	node->m_ptrParent = root;

	if (NODE_FILE == type)
	{
		m_pCurFile = node;
	}

	return true;
}

void CTree::ShowTree(void)
{
	//m_treeRoot->ShowNode();
}

TREE_API IDiskStruct * CreateTree()
{
	return static_cast<IDiskStruct*> (new CTree());
}

String &CTree::GetCurDir()
{
	return m_strActiveDirName;
}

/*
 *
 *type 为1时表示向子目录更新，为2时表示向父目录更新
 *
 */
void CTree::UpdateCurDir(const char *str, unsigned type)
{

	if (1 == type)
	{
		m_strActiveDirName = m_strActiveDirName.substr(0, m_strActiveDirName.find('>'));
		if (m_pCurDir == m_treeRoot)
		{
			m_strActiveDirName = m_strActiveDirName + str + ">";
		}
		else
		{
			m_strActiveDirName = m_strActiveDirName + "\\" + str + ">";
		}
	}
	else if (2 == type)
	{
		if (m_pCurDir->m_ptrParent != m_treeRoot)
		{
			m_strActiveDirName = m_strActiveDirName.substr(0, m_strActiveDirName.rfind('\\'));
		}
		else
		{
			m_strActiveDirName = m_strActiveDirName.substr(0, m_strActiveDirName.rfind('\\') + 1);
		}

		m_strActiveDirName = m_strActiveDirName + ">";
	}
}

bool CTree::ChangeDir(const char *str)
{
	if (strcmp(str, ".") == 0)
	{
		return true;
	}
	if (strcmp(str, "..") == 0)
	{
		if(m_pCurDir != m_treeRoot)
		{
			UpdateCurDir(str, 2);
			m_pCurDir = m_pCurDir->m_ptrParent;
		}
		return true;
	}

	for (unsigned i = 0; i < m_pCurDir->m_uChildCount; i++)
	{
		if (m_pCurDir->m_ptrChild[i]->m_strData == str)
		{
			UpdateCurDir(str, 1);
			m_pCurDir = m_pCurDir->m_ptrChild[i];
			return true;
		}
	}

	return false;
}

bool CTree::DestroyTree(void)
{
	if (NULL != m_treeRoot)
	{
		delete m_treeRoot;
		m_treeRoot = NULL;
	}

	return true;
}

CTreeNode * CTree::GetCurNode(void)
{
	return m_pCurDir;
}

bool CTree::CreateDir(String &str)
{
	return Add(m_pCurDir, str, NODE_DIR);;
}

bool CTree::ShowCurDirContent(const char *str)
{
	char temp[4096] = "";		//如果子文件夹居多，可能会出现溢出崩溃现象
	char curdir [512];
	char ss[512];

	unsigned file = 0;
	unsigned dir = 0;
	unsigned buf = 0;		

	strcpy(curdir, (m_strActiveDirName.substr(0, m_strActiveDirName.find('>'))).c_str());

	if (strcmp(str, "") == 0)	//0
	{				
		dir = 2;
		sprintf(temp, "\n%s 的目录\n\n\t<DIR>\t\t.	\n\t<DIR>\t\t..	\n", curdir);
		if (NULL == m_pCurDir->m_ptrChild)
		{
			strcat(temp, "\t0 个文件              0 字节\n \t2 个目录\n");
		}
		else
		{	
			m_pCurDir->ShowNode(temp, 0);
		}
	}
	else if (stricmp(str, "/s") == 0)	//1 递归汇总
	{
		m_pCurDir->ShowNodeDeep(m_pCurDir, temp, 1, file, dir, buf, curdir);
		sprintf(ss, "\n\n\t共有%d 个文件              %d 字节\n \t共有%d 个目录\n", file, buf, dir);
		strcat(temp, ss);
	}
	else if (stricmp(str, "/ad") == 0)	//2只显示目录
	{
		dir = 2;
		strcpy(curdir, (m_strActiveDirName.substr(0, m_strActiveDirName.find('>'))).c_str());
		sprintf(temp, "\n%s 的目录\n\n\t<DIR>          .	\n\t<DIR>          ..	\n", curdir);
		m_pCurDir->ShowNode(temp, 2);
	}
	else if (stricmp(str, "/af") == 0)	//3只显示文件
	{
		dir = 2;
		strcpy(curdir, (m_strActiveDirName.substr(0, m_strActiveDirName.find('>'))).c_str());
		sprintf(temp, "\n%s 的目录\n\n\t<DIR>          .	\n\t<DIR>          ..	\n", curdir);
		m_pCurDir->ShowNode(temp, 3);
	}
	else if (stricmp(str,"/ad /s") ==0 || stricmp(str, "/s /ad") == 0)	//4递归只显示目录
	{
		m_pCurDir->ShowNodeDeep(m_pCurDir, temp, 4, file, dir, buf, curdir);
		sprintf(ss, "\n\n\t共有%d 个文件              %d 字节\n \t共有%d 个目录\n", file, buf, dir);
		strcat(temp, ss);
	}
	else if (stricmp(str,"/af /s") ==0 || stricmp(str, "/s /af") == 0)	//5递归只显示文件
	{
		m_pCurDir->ShowNodeDeep(m_pCurDir, temp, 5, file, dir, buf, curdir);
		sprintf(ss, "\n\n\t共有%d 个文件              %d 字节\n \t共有%d 个目录\n", file, buf, dir);
		strcat(temp, ss);
	}

	ShowString(temp);
	 return true;
}

bool CTree::ReMoveDir(const char *str)
{	
	if (stricmp(str, "/s") == 0)
	{
		DelDeep();	//递归删除
		return true;
	}
	else
	{
		return DelNode(m_pCurDir,str, NODE_DIR);
	}
}

bool CTree::DelNode(CTreeNode *root, const char *str, NODE_TYPE type)
{
	if (strcmp(str, "") == 0)
	{
		return true;
	}

	for (unsigned i = 0; i < root->m_uChildCount; i++)
	{
		if (root->m_ptrChild[i]->m_strData == str && root->m_ptrChild[i]->m_emNodeType == type)
		{
			delete root->m_ptrChild[i];
			do 
			{
				if (i == root->m_uChildCount - 1)
				{
					root->m_ptrChild[i] = NULL;
					break;
				}
				else
				{
					root->m_ptrChild[i] = root->m_ptrChild[i + 1];
				}
				i ++;
			} while (i < root->m_uChildCount);

			root->m_uChildCount --;

			if (0 == root->m_uChildCount)
			{
				delete [] root->m_ptrChild;
				root->m_ptrChild = NULL;
				root->m_uNodeSum = 0;
			}

			return true;
		}
	}

	return false;
}

bool CTree::CopyTrueFile(const char *str)
{

	char *strPath = NULL, *strName = NULL;
	char *first = strtok(const_cast<char *>(str), " ");
	char *second = strtok(NULL, " ");
	if (NULL != second)
	{
		strPath = first;
		strName = second;
	}
	else
	{
		const char *p = strrchr(str, '/');
		if (p == NULL)
		{
			return false;
		}

		if(strcmp((p+1), "*") == 0)
		{
			return CopyAll(str);
		}

		if (access(str, 0) != 0 || access(str, 6) != 0)
		{
			return false;
		}
		
		strPath = const_cast<char *>(str);
		strName = const_cast<char *>(p + 1);
	}

	

	//如果是文件夹 报错
	DWORD att = GetFileAttributes(strPath);
	if (FILE_ATTRIBUTE_DIRECTORY == att)
	{
		return false;
	}

	String strFileName =  strName;

	return CopyOneFile(str, strFileName.c_str());
}

bool CTree::DelFile(const char *str)
{
	if (strcmp(str, "*") == 0)
	{
		DelAllFile();
		return true;
	}
	else
	{
		return DelNode(m_pCurDir, str, NODE_FILE);
	}
}

bool CTree::CompareFile(const char *str)
{
	//先不考虑文件、文件夹的名称中带有空格
	String strFile = str;
	String strBuf = strFile.substr(strFile.find(' ') + 1);
	strFile = strFile.substr(0, strFile.find(' '));

	CTreeNode * node = FindNode(strBuf.c_str(), NODE_FILE);
	if (NULL == node || NULL == node->m_buff)
	{
		return false;
	}

	char *buff = NULL;
	unsigned len = 0;
	try
	{
		ifstream icin;
		icin.open(strFile.c_str(), ios::in | ios::binary);
		if(icin.bad())
		{
			return false;
		}

		icin.seekg (0, ios::end);
		len = icin.tellg();
		buff = new char[len];
		icin.seekg(0, ios::beg);
		icin.read (buff, len);

		icin.close();
	}
	catch(exception &e)
	{
		m_pCtrl->ShowString(e.what());
		return false;
	}

	unsigned index = 0;
	while(node->m_buff[index] == buff[index] && 
		index < (len > node->m_uBufLen ? node->m_uBufLen : len))
	{
		index ++;
	}

	char temp[1024];
	if (index >= len && index >= node->m_uBufLen)
	{
		ShowString("\n\t内容一致\n");
	}
	else
	{
		sprintf(temp, "\n位置 %d\n", index);
		ShowString(temp);
		ShowString("\n");

		ShowString("ASCII:\t");
		for (int k = 0; k < 16 && k + index < len; k++)
		{
			sprintf(temp, "%c", buff[k + index]);
			ShowString(temp);
		}

		ShowString("\n");		
		ShowString("HEX:\t");
		for (int x = 0; x < 16 && x + index < len; x++)
		{
			sprintf(temp, "%.x", buff[x + index]);
			ShowString(temp);
		}
		
		ShowString("\n");
		ShowString("\n");
		ShowString("ASCII:\t");
		for (int i = 0; i < 16 && i + index < node->m_uBufLen; i++)
		{
			sprintf(temp, "%c", node->m_buff[i + index ]);
			ShowString(temp);
		}

		ShowString("\n");
	ShowString("HEX:\t");
		for (int j = 0; j < 16 && j + index < node->m_uBufLen; j++)
		{
			sprintf(temp, "%.2x", node->m_buff[j + index]);
			ShowString(temp);
		}		

		ShowString("\n");
	}

	delete [] buff;
	buff = NULL;

	return true;
}

void CTree::ShowString(const char *str)
{
	m_pCtrl->ShowString(str);
}

CTreeNode * CTree::FindNode(const char *str, NODE_TYPE type)
{
	return FindNode(str, type, m_pCurDir);
}

CTreeNode * CTree::FindNode(const char *str, NODE_TYPE type, CTreeNode *root)
{
	for (unsigned i = 0; i < root->m_uChildCount; i++)
	{
		if (root->m_ptrChild[i]->m_strData == str && 
			root->m_ptrChild[i]->m_emNodeType == type)
		{
			return root->m_ptrChild[i];
		}
	}

	return NULL;
}

bool CTree::DelDeep()
{
	unsigned num = m_pCurDir->m_uChildCount;
	for (unsigned i = 0; i < num; i++)
	{
		if (NODE_DIR == m_pCurDir->m_ptrChild[i]->m_emNodeType &&
			m_pCurDir->m_ptrChild[i]->m_strData != ".." && m_pCurDir->m_ptrChild[i]->m_strData != ".")
		{
			delete m_pCurDir->m_ptrChild[i];
			m_pCurDir->m_ptrChild[i] = NULL;
			m_pCurDir->m_uChildCount --;
		}
	}

	return true;
}

void CTree::DelAllFile()
{
	unsigned num = m_pCurDir->m_uChildCount;
	for (unsigned i = 0; i < num; i++)
	{
		if (NODE_FILE == m_pCurDir->m_ptrChild[i]->m_emNodeType)
		{
			delete m_pCurDir->m_ptrChild[i];
			m_pCurDir->m_ptrChild[i] = NULL;
			m_pCurDir->m_uChildCount --;
		}
	}
}	

bool CTree::CopyAll(const char *str)
{
	char temp[512];
	char strFullName[512];
	strcpy(temp, str);
	*(strrchr(temp, '/') + 1) = 0;

	//循环加载dll
	WIN32_FIND_DATAA wfd;
	HANDLE hFind = FindFirstFile(str, &wfd);
	if(INVALID_HANDLE_VALUE == hFind)
		return false;
	do
	{		
		strcpy(strFullName, temp);
		strcat(strFullName, wfd.cFileName);
		DWORD att = GetFileAttributes(strFullName);
		att = att&FILE_ATTRIBUTE_DIRECTORY;
		if (FILE_ATTRIBUTE_DIRECTORY == att)
		{
			continue;
		}
		CopyOneFile(strFullName, wfd.cFileName);

	}while(FindNextFile(hFind, &wfd));

	return true;
}

bool CTree::CopyOneFile(const char *strPath, const char *strFileName)
{
	if(!Add(m_pCurDir, String(strFileName), NODE_FILE))
	{
		return false;
	}

	try
	{
		ifstream icin;
		icin.open(strPath, ios::in | ios::binary);
		if(icin.bad())
		{
			return false;
		}

		icin.seekg (0, ios::end);
		m_pCurFile->m_uBufLen = icin.tellg();
		m_pCurFile->m_buff = new char[m_pCurFile->m_uBufLen];
		icin.seekg(0, ios::beg);
		icin.read (m_pCurFile->m_buff, m_pCurFile->m_uBufLen);

		icin.close();
	}
	catch(exception &e)
	{
		m_pCtrl->ShowString(e.what());
		return false;
	}

	return true;
}
