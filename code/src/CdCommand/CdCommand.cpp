// CdCommand.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "CdCommand.h"

CDCOMMAND_API const char *GetCommandName(void)
{
	return "cd";
}


CDCOMMAND_API ICommand *CreateCommand(IControl *ctrl)
{
	return static_cast<ICommand*>(new CCdCommand(ctrl));
}


CCdCommand::CCdCommand(IControl *ctrl)
{
	m_pControl = ctrl;
}

bool CCdCommand::Execute(const char *str, ...)
{
	char temp[512];
	//切换目录
	if (strcmp(str, "?") == 0)
	{
		Help();
	}
	else
	{
		if (CheckParam(str))
		{
			if(!m_pControl->ChangeDir(str))
			{
				sprintf(temp, "错误的路径: %s\n", str);
				m_pControl->ShowString(temp);
				return false;
			}
		}
	}

	return true;
}

const char *CCdCommand::Describe()
{
	return "CD\t\t\t显示当前目录的名称或将其更改。\n";
}

void CCdCommand::Help(void)
{
	m_pControl->ShowString("CD [path]\nCD [..]\n..   指定要改成父目录。\n");
}

bool CCdCommand::CheckParam(const char*str)
{
	if (strcmp(str, "") == 0)
	{
		return false;
	}

	return true;
}

