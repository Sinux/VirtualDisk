// DirCommand.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "DirCommand.h"

CDirCommand::CDirCommand(IControl *ctrl)
{
	m_pControl =ctrl;
}

CDirCommand::~CDirCommand()
{

}

bool CDirCommand::Execute(const char *str, ...)
{
	if (strcmp(str, "?") == 0)
	{
		Help();
	}
	else
	{
		if (CheckParam(str))
		{
			m_pControl->ShowCurDirContent(str);
		}
	}

	return true;
}

const char * CDirCommand::Describe()
{
	return "DIR\t\t\t显示一个目录中的文件和子目录。\n";
}

void CDirCommand::Help(void)
{
	m_pControl->ShowString("显示目录中的文件和子目录列表。\n\nDIR [path][filename] [/A[[:]attributes]] [/S] \n \
	指定要列出的驱动器、目录和/或文件。\n \
	/A          显示具有指定属性的文件。\n \
	属性\t\tD  目录\t\tF  文件\n\n \
	/S          显示指定目录和所有子目录中的文件。\n");  
}

bool CDirCommand::CheckParam(const char *str)
{
#define NUM 2
#define LENGTH 20
	char strErr[1024];
	char arr[NUM][LENGTH] = {""};
	strcpy(strErr, str);
	const char *tokenPtr=strtok((char *)(strErr)," ");
	unsigned i = 0;
	bool bFlag = true;

	while(tokenPtr !=NULL && i < NUM)
	{
		strncpy(arr[i ++], tokenPtr, LENGTH);
		tokenPtr=strtok(NULL," ");
	}

	if (tokenPtr != NULL)
	{
		bFlag = false;
	}
	
	for (int i = 0; i < NUM && strcmp(arr[i], "") != 0; i++)
	{
		if(!IsInArr(arr[i]))
		{
			bFlag = false;
			break;
		}
	}

	if (!bFlag)
	{
		sprintf(strErr, "不能识别的参数: '%s'\n", str);
		m_pControl->ShowString(strErr);
	}

	return bFlag;
#undef NUM
#undef LENGTH
}

bool CDirCommand::IsInArr(const char *str)
{
	char arr[][5] = {"/af", "/ad", "/s"};
	unsigned index = 0;
	for (;index < sizeof(arr) / sizeof(char *); index++)
	{
		if (!stricmp(arr[index], str))
		{
			break;
		}
	}

	if (index >= sizeof(arr) / sizeof(char *))
	{
		return false;
	}

	return true;
}

DIRCOMMAND_API const char * GetCommandName(void)
{
	return "dir";
}

DIRCOMMAND_API ICommand * CreateCommand(IControl *ctrl)
{
	return static_cast<ICommand*>(new CDirCommand(ctrl));
}
