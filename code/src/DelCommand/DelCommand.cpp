// DelCommand.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "DelCommand.h"

CDelCommand::CDelCommand(IControl *ctrl)
{
	m_pControl = ctrl;
}

CDelCommand::~CDelCommand()
{
	
}

bool CDelCommand::Execute(const char *str, ...)
{
	char temp[512];
	if (strcmp(str, "?") == 0)
	{
		Help();
	}
	else
	{
		if(!m_pControl->DelFile(str))
		{
			sprintf(temp, "错误的参数: %s\n", str);
			m_pControl->ShowString(temp);
		}
	}
	return true;
}

const char * CDelCommand::Describe()
{
	return "DEL\t\t\t删除至少一个文件。\n";
}

void CDelCommand::Help()
{
	m_pControl->ShowString("删除一个或数个文件。\n\
		DEL name\n\
		names         指定一个文件或者目录列表。\n\
		通配符可用来删除多个文件。\n\
		如果指定了一个目录，该目录中的所\n\
		有文件都会被删除。\n");
}

bool CDelCommand::CheckParam(const char *)
{
	return true;
}

DELCOMMAND_API const char * GetCommandName(void)
{
	return "del";
}

DELCOMMAND_API ICommand * CreateCommand(IControl *ctrl)
{
	return static_cast<ICommand*>(new CDelCommand(ctrl));
}
