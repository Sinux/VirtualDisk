// CopyCommand.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "CopyCommand.h"

CCopyCommand::CCopyCommand(IControl *ctrl)
{
	m_pControl = ctrl;
}

CCopyCommand::~CCopyCommand()
{
	
}

bool CCopyCommand::Execute(const char *str, ...)
{
	char temp[512];
	if (strcmp(str, "?") == 0)
	{
		Help();
	}
	else
	{
		if (!CheckParam(str))
		{
			Help();
			return false;
		}

		String s = str;
		s.replace('\\', '/');
		if(!m_pControl->CopyTrueFile(s.c_str()))
		{
			sprintf(temp, "文件不存在或者没有访问权限: %s\n", str);
			m_pControl->ShowString(temp);
			return false;
		}
	}
	return true;
}

const char * CCopyCommand::Describe()
{
	return "COPY\t\t\t将至少一个文件复制到另一个位置。\n";
}

void CCopyCommand::Help()
{
	m_pControl->ShowString("将一份或多份文件复制到另一个位置。\nCOPY 硬盘文件路径  虚拟磁盘中的文件名\n\
COPY 硬盘文件路径\nCOPY 硬盘目录\\*\n");
}

bool CCopyCommand::CheckParam(const char *str)
{
	if (strcmp(str, "") == 0)
	{
		return false;
	}
	return true;
}

COPYCOMMAND_API ICommand * CreateCommand(IControl *ctrl)
{
	return static_cast<ICommand*>(new CCopyCommand(ctrl));
}

COPYCOMMAND_API const char * GetCommandName(void)
{
	return "copy";
}
