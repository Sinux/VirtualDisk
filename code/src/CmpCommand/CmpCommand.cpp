// CmpCommand.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include <io.h>
#include "CmpCommand.h"

CCmpCommand::CCmpCommand(IControl *ctrl)
{
	m_pControl = ctrl;
}

bool CCmpCommand::Execute(const char *str, ...)
{
	char temp[512];
	if (strcmp(str, "?") == 0)
	{
		Help();
	}
	else
	{
		if (!CheckParam(str))
		{
			sprintf(temp, "参数错误: %s\n", str);
			m_pControl->ShowString(temp);
			return false;
		}
		else
		{
			if(!m_pControl->CompareFile(str))
			{
				sprintf(temp, "参数错误: %s\n", str);
				m_pControl->ShowString(temp);
				return false;
			}
		}
	}
	return true;
}

const char *CCmpCommand::Describe()
{
	return "CMP\t\t\t比较两个或两套文件的内容。\n";
}

void CCmpCommand::Help(void)
{
	m_pControl->ShowString("比较两个文件或两个文件集的内容。\n \
		CMP [data1] [data2]\n \
		data1      指定要比较的真实硬盘的文件路径。\n \
		data2      指定要比较的虚拟磁盘文件名称。\n");
}

bool CCmpCommand::CheckParam(const char *str)
{
	String strTemp = str;
	strTemp = strTemp.substr(0, strTemp.find(' '));

	//检测路径
	if (access(strTemp.c_str(), 0) != 0 || access(strTemp.c_str(), 6) != 0)
	{
		return false;
	}

	//如果是文件夹 报错
	DWORD att = GetFileAttributes(strTemp.c_str());
	if (FILE_ATTRIBUTE_DIRECTORY == att)
	{
		return false;
	}

	return true;
}

CMPCOMMAND_API const char * GetCommandName(void)
{
	return "cmp";
}

CMPCOMMAND_API ICommand * CreateCommand(IControl *ctrl)
{
	return static_cast<ICommand*>(new CCmpCommand(ctrl));
}
