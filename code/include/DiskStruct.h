#ifndef DISKSTRUCT_H
#define DISKSTRUCT_H

#include "CString.h"
#include "Control.h"

/*
	@brief磁盘存储结构的抽象类
*/
class IDiskStruct
{
protected:
	IControl *m_pCtrl;
public:
	IDiskStruct(){m_pCtrl = 0;}
	virtual ~IDiskStruct(){}
	virtual bool Init(IControl *) = 0;
	virtual bool Add() = 0;
	virtual String &GetCurDir() = 0;
	virtual bool ChangeDir(const char *) = 0;
	virtual bool CreateDir(String &) = 0;
	virtual bool ShowCurDirContent(const char *str) = 0;
	virtual bool ReMoveDir(const char *) = 0;
	virtual bool CopyTrueFile(const char *) = 0;
	virtual bool DelFile(const char *) = 0;
	virtual bool CompareFile(const char *) =0;
}; 

#endif // DISKSTRUCT_H
