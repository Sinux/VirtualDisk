#ifndef DISKUI_H
#define DISKUI_H

#include "CString.h"

class IDiskUI
{
public:
	virtual ~IDiskUI(){}
	virtual String GetCommand() = 0;
	virtual void OutputString(String &) = 0;
	virtual void OutputString(const char *) = 0;
};



#endif