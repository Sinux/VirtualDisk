#ifndef CONTROL_H
#define CONTROL_H

#include "CString.h"

class IControl
{
public:
	IControl(void){}
	virtual ~IControl(void){}
	virtual bool ShowString(String &) = 0;
	virtual bool ShowString(const char *) = 0;
	virtual bool ChangeDir(const char *) = 0;
	virtual bool CreateDir(const char *) = 0;
	virtual bool ShowCurDirContent(const char *) = 0;
	virtual bool ReMoveDir(const char *) = 0;
	virtual bool CopyTrueFile(const char *) = 0;
	virtual bool DelFile(const char *) = 0;
	virtual bool CompareFile(const char *) =0;
};

#endif