#ifndef VTLOG_H
#define VTLOG_H

#ifdef WIN32
	#define KDM_API  extern "C" __declspec(dllexport)
	#define STDCALL  __stdcall
#else
	#define KDM_API
	#define STDCALL 
#endif

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#if (defined(_WIN32) || defined(_WIN64)) && !defined(__WIN__)
#define __WIN__
#endif

#if !defined(__WIN__)

#define STDCALL		/**<  */
#define X_API		/**<  */
#define CALLBACK	/**<  */
#ifndef FALSE		
#define FALSE 0		/**<  */
#endif
#ifndef TRUE		
#define TRUE 1		/**<  */
#endif
#ifndef HWND		
#define HWND void*	/**<  */
#endif
#ifndef HDC
#define HDC void*	/**<  */
#endif

#else

#include <windows.h>
#define STDCALL __stdcall	/**<  */
#define X_API __declspec(dllexport)	/**<  */
#define CALLBACK __stdcall	/**<  */
#define _CRT_SECURE_NO_WARNINGS 1 /**<  */
#if defined(_MSC_VER)
#define snprintf _snprintf	/**<  */
#endif

#endif

#ifdef __WIN__
typedef struct {
	HANDLE hthread;
} LThread;

typedef struct {
	CRITICAL_SECTION section;
} LMutex;
#else
#include <pthread.h>
typedef struct {
	pthread_t thr;
} LThread;

typedef struct {
	pthread_mutex_t mutex;
} LMutex;
#endif

#ifndef OUT
#define OUT		/**<  */
#endif

#ifndef IN
#define IN		/**<  */
#endif

#ifndef NULL
#define NULL 0		/**<  */
#endif

#ifndef MAX_PATH
#define MAX_PATH                            260
#endif

/**    
    @name	通用基本数据类型
    @{
*/
//typedef int BOOL;		/**<  */
#define BOOL int
typedef char int8;		/**<  */
typedef unsigned char uint8;		/**<  */
typedef short int16;		/**<  */
typedef unsigned short uint16;		/**<  */
typedef long int32;		/**<  */
typedef unsigned long uint32;		/**<  */
typedef long long int int64;		/**<  */
typedef unsigned long long uint64;		/**<  */
/**
    @}
*/

X_API void STDCALL
log_buf_lock();

X_API void STDCALL
log_buf_date();

X_API void STDCALL
log_buf_msg(const char *fmt, ...);

X_API void STDCALL
log_buf_msg_unlock(const char *fmt, ...);

X_API void STDCALL
log_get_path(OUT char* path);

X_API void STDCALL
log_set_path(char* path);

#define vtlog log_buf_lock();log_buf_date();log_buf_msg("file: %s line: %d ", __FILE__, __LINE__);log_buf_msg_unlock


/**
    @addtogroup 互斥体
    @{
*/
/**
    @brief 初始化互斥体 
    @param mutex 互斥体句柄
    @param recursive 是否支持自旋
    @retval TRUE 成功 
    @retval FALSE 失败
 */
X_API BOOL STDCALL
os_mutex_init(LMutex* mutex, BOOL recursive);

X_API LMutex* STDCALL
os_mutex_create(BOOL recursive);

X_API BOOL STDCALL
os_mutex_free(LMutex* mutex);

/**
    @brief 销毁互斥体 
    @param mutex 互斥体句柄
    @retval TRUE 成功 
    @retval FALSE 失败
 */
X_API BOOL STDCALL
os_mutex_destroy(LMutex* mutex);

/**
    @brief 锁定互斥体
    @param mutex 互斥体句柄
    @retval TRUE 成功 
    @retval FALSE 失败
 */
X_API BOOL STDCALL
os_mutex_lock(LMutex* mutex);

/**
    @brief 解锁互斥体
    @param mutex 互斥体句柄
    @retval TRUE 成功 
    @retval FALSE 失败
 */
X_API BOOL STDCALL
os_mutex_unlock(LMutex* mutex);
/**
    @}
*/

#ifdef __cplusplus
}
#endif

#endif
