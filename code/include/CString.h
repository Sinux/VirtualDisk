#ifndef STRING_H
#define STRING_H

#include <iostream>

using namespace std;

class String
{
public:
	//构造函数
	String(void);
	String(const char * str);
	String(String &str);

	//析构函数
	~String(void);

	//赋值函数
	String & operator = (String &str);
	String & operator = (const char * str);
	bool operator == (String &str);
	bool operator == (const char * str);
	bool operator != (String &str);
	bool operator != (const char * str);


	//运算符重载
	char & operator [](unsigned index);
	String & operator + (String &str);
	String & operator + (const char * str);
	friend ostream & operator << (ostream &os, String &str)
	{
		os<<str.c_str();
		return os;
	}

	friend istream & operator >> (istream &is, String &str)
	{
		if (NULL != str.m_str)
		{
			delete [] str.m_str;
		}
		
		str.m_str = new char[1024];
		str.m_length = 255;

		try
		{
			is.clear();		//清除错误
			is.getline(str.m_str, 256);
			is.clear();		//清除错误
			is.sync_with_stdio(false);	//直接从std读取，不经过缓冲区
			is.sync();		//清空缓冲区
		}
		catch(exception &e)
		{
			cout<<e.what()<<endl;
		}

		str.m_length = strlen(str.m_str);

		return is;
	}

	//普通功能函数
	unsigned length(void);
	const char * c_str();
	String & append(const char *str);
	bool empty(void);
	int find(const char ch);
	int find(String &str);
	int find(const char *str);
	int rfind(const char ch);
	int rfind(String &str);
	int rfind(const char *str);
	String substr(unsigned, unsigned uEnd = 0);
	void trim(void);
	unsigned replace(char, char);


private:
	char *m_str;		//存储字符串
	unsigned m_length;	//字符串长度(不含\0)
};

#endif
