// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 COMMANDUI_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// COMMANDUI_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。
#ifndef COMMANDUI_H
#define COMMANDUI_H

#ifdef COMMANDUI_EXPORTS
#define COMMANDUI_API extern "C" __declspec(dllexport)
#else
#define COMMANDUI_API extern "C" __declspec(dllimport)
#endif

#include "DiskUI.h"

class  CCommandUI : public IDiskUI
{
public:
	CCommandUI(void);
	~CCommandUI();

	//重写
	virtual String GetCommand();
	virtual void OutputString(String &);
	virtual void OutputString(const char *);
};

COMMANDUI_API IDiskUI* _stdcall CreateCommandUI(void);

#endif