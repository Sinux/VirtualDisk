#ifndef COMMAND_H
#define COMMAND_H

#pragma warning(disable : 4996)		//屏蔽警告
/**
	所有实现ICommand的类 还要实现GetCommandName、CreateCommand的导出函数
*/
#include "Control.h"
/*
	命令的抽象类
*/
class ICommand 
{
protected:
	IControl *m_pControl;
public:
	ICommand(){m_pControl = 0;}
	virtual ~ICommand(){}
	virtual bool Execute(const char *, ...) = 0;
	virtual void Help(void) = 0;
	virtual const char *Describe() = 0;
	virtual bool CheckParam(const char *) = 0;
};


#endif